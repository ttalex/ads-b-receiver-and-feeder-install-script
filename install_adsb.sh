#!/bin/bash

# Developed by ttalex aka Satbeams
# Based on original script by abcd567
# Inspired by https://discussions.flightaware.com/t/burning-out-sd-cards/52000

# General config files settings
DUMP1090_FILE=/etc/default/dump1090-fa
PA_FILE=/boot/piaware-config.txt
PA_CONF_FILE=/etc/piaware.conf
PF_FILE=/etc/pfclient-config.json
FR24_FILE=/etc/fr24feed.ini
RB24_FILE=/etc/rbfeeder.ini
OS_SERIAL_FILE=/var/lib/openskyd/conf.d/05-serial.conf
OS_CONF_FILE=/var/lib/openskyd/conf.d/10-debconf.conf
NW_CONF_FILE=/etc/network/interfaces

# Miscellaneous settings
HeyWhatsThatID=''
MyBingMapsID=''

# Receiver settings
MyLat=''
MyLon=''
MyAlt='100'
MyGain='-10'
# Set your receiver's IP address if feed from another box otherwise keep 127.0.0.1
MyReceiverHost='127.0.0.1'
MyReceiverPort='30005'

# Set your Feeder IP if you wish to skip DHCP and assign fixed address at OS level otherwise leave empty
MyFixedIP=''

# PIAware feeder settings - set to '' to skip
MyFAFeederID=''

# PlaneFinder feeder settings - set to '' to skip
MyPFSharerCode=''

# FlightRadar24 feeder settings - set to '' to skip
MyFR24KEY=''

# RadarBox24 feeder settings - set to '' to skip
MyRB24KEY=''
MyRBSN='EXTRPI00XXXX'

# OpenSkyNetwork feeder settings - set to '' to skip
MyOSSerial=''
MyOSUser='UserXXXX'

echo -e "\e[33mUpdating.....\e[39m"
sudo apt-get update

## (0) Set Fixed IP address
if [[ ! -z $MyFixedIP ]]
then
# Update Network Interfaces file
echo -e "\e[33mSetting Feeder IP address......\e[39m"
sudo cat <<EOT > "$NW_CONF_FILE"
allow-hotplug eth0
iface eth0 inet static
address $MyFixedIP
netmask 255.255.255.0
gateway 192.168.1.1
dns-nameservers 192.168.1.1 8.8.4.4

EOT
fi

echo -e "\e[33mInstalling required libraries.....\e[39m"
# apt-get install libboost-all-dev
# install CIFS support
# sudo apt-get install -y cifs-utils --fix-missing
# sudo mkdir /media/share

## (1) DUMP1090-FA INSTALLATION
if [[ $MyReceiverHost == '127.0.0.1' ]]
then
# Install Dump1090-FA
echo -e "\e[33minstalling dump1090-fa......\e[39m"

sudo touch $DUMP1090_FILE
sudo chmod 644 $DUMP1090_FILE
sudo cat <<EOT > "$DUMP1090_FILE"
# dump1090-fa configuration
# This is sourced by /usr/share/dump1090-fa/start-dump1090-fa as a
# shellscript fragment.

# If you are using a PiAware sdcard image, this config file is regenerated
# on boot based on the contents of piaware-config.txt; any changes made to this
# file will be lost.

# dump1090-fa won't automatically start unless ENABLED=yes
ENABLED=yes

# Allowed Gain values: 20.7 22.9 25.4 28.0 29.7 32.8 33.8 -10 36.4 37.2 38.6 40.2 42.1 43.4 43.9 44.5 48.0 49.6
RECEIVER_OPTIONS="--device-index 0 --gain $MyGain --ppm 0 --modeac --forward-mlat --lat $MyLat --lon $MyLon "
DECODER_OPTIONS="--max-range 360 --fix"
NET_OPTIONS="--net --net-heartbeat 60 --net-ro-size 1300 --net-ro-interval 0.2 --net-ri-port 0 --net-ro-port 30002 --net-sbs-port 30003 --net-bi-port 30004,30104 --net-bo-port 30005"
JSON_OPTIONS="--json-location-accuracy 2"

EOT

sudo wget https://flightaware.com/adsb/piaware/files/packages/pool/piaware/p/piaware-support/piaware-repository_3.8.0_all.deb
sudo dpkg -i piaware-repository_3.8.0_all.deb 
sudo apt-get update 
sudo apt-get install -y dump1090-fa 
# Make more circular lines:
sudo sed -i.original 's/SiteCirclesDistances = new Array(100,150,200);/SiteCirclesDistances = new Array(100,150,200,250,300);/g' /usr/share/dump1090-fa/html/config.js
# This one changes the terrain limit ring to blue, increases the thickness and makes it dashed rather than solid:
sudo perl -0777 -i.original -pe "s/var\s*ringStyle{1}\s*=\s*new\s*ol\.style\.Style{1}\(\{\n\s*fill:\s*null,\n\s*stroke:\s*new\s*ol\.style\.Stroke{1}\(\{\n\s*color{1}:\s*'#000000',\n\s*width{1}:\s*1\n\s*\}\)\n\s*\}\);{1}/var ringStyle = new ol.style.Style\(\{\nfill: null,\nstroke: new ol.style.Stroke\(\{\n color: '#0000DD',\n lineDash:[2,2],\nwidth: 1\n\}\)\n\}\);\n/igs" /usr/share/dump1090-fa/html/script.js
## TERRAIN LIMIT RINGS
if [[ ! -z "$HeyWhatsThatID" ]]
then
echo -e "\e[33mAdding Terrain Limit Ring ....\e[39m"
sudo wget -O /usr/share/dump1090-fa/html/upintheair.json "http://www.heywhatsthat.com/api/upintheair.json?id=$HeyWhatsThatID&refraction=0.25&alts=3000,6000,9000,12000"  
fi
echo -e "\e[32mInstallation of dump1090-fa completed ....\e[39m"
## WIEDEHOPF's GRAPHS INSTALLATION
echo -e "\e[33mInstalling wiedehopf's Graphs....\e[39m"
sudo bash -c "$(wget -q -O - https://raw.githubusercontent.com/wiedehopf/graphs1090/master/install.sh)"
echo -e "\e[32mInstallation of Graphs completed...\e[39m"
fi

## (2) PIAWARE INSTALLATION
if [[ ! -z "$MyFAFeederID" ]]
then
if [[ $MyReceiverHost != '127.0.0.1' ]]
then
# Install PIAware repository
echo -e "\e[33minstalling PIAware repository......\e[39m"
sudo wget https://flightaware.com/adsb/piaware/files/packages/pool/piaware/p/piaware-support/piaware-repository_3.8.0_all.deb
sudo dpkg -i piaware-repository_3.8.0_all.deb 
sudo apt-get update 
fi
echo -e "\e[33minstalling Piaware data feeder......\e[39m"
sudo apt-get install -y piaware 
sudo piaware-config allow-auto-updates yes 
sudo piaware-config allow-manual-updates yes 
sudo piaware-config feeder-id $MyFAFeederID
sudo piaware-config receiver-host $MyReceiverHost
sudo piaware-config receiver-port $MyReceiverPort
sudo systemctl restart piaware
echo -e "\e[32mPiaware Data feeder Installation and configuration completed ....\e[39m"
fi

## (3) PLANEFINDER DATA FEEDER INSTALLATION
if [[ ! -z "$MyPFSharerCode" ]]
then
echo -e "\e[33minstalling Planefider data feeder.......\e[39m"
wget http://client.planefinder.net/pfclient_4.1.1_armhf.deb
sudo dpkg -i pfclient_4.1.1_armhf.deb
## Setting up pfclient configuration
sudo chmod 666 $PF_FILE
sudo cat <<EOT > "$PF_FILE"
{
"tcp_address":"$MyReceiverHost",
"tcp_port":"$MyReceiverPort",
"select_timeout":"10",
"data_upload_interval":"10",
"connection_type":"1",
"aircraft_timeout":"30",
"data_format":"1",
"latitude":"$MyLat",
"longitude":"$MyLon",
"sharecode":"$MyPFSharerCode"
}

EOT

sudo systemctl restart pfclient
echo -e "\e[32mInstallation and configuration of PFClient completed.....\e[39m"
fi

## (4) RB24FEEDER INSTALLATION
if [[ ! -z "$MyRB24KEY" ]]
then
echo -e "\e[33mInstalling RadarBox24 feeder.....\e[39m"
sudo bash -c "$(wget -O - http://apt.rb24.com/inst_rbfeeder.sh)"

## RB24FEEDER configuration
sudo touch $RB24_FILE
sudo chmod 666 $RB24_FILE
sudo cat <<EOT > "$RB24_FILE"
[client]
network_mode=true
log_file=/var/log/rbfeeder.log
key=$MyRB24KEY
sn=$MyRBSN

lat=$MyLat
lon=$MyLon
alt=$MyAlt

sat_used=0
sat_visible=0

[network]
mode=beast
external_port=$MyReceiverPort
external_host=$MyReceiverHost

[mlat]
autostart_mlat=true

EOT

## MLAT-CLIENT INSTALLATION (FOR RB24 FEEDER)
echo -e "\e[33mInstalling mlat-client for rb24 feeder.....\e[39m"
sudo apt-get install -y git curl build-essential debhelper python-dev python3-dev
cd /home/pi/
git clone https://github.com/mutability/mlat-client.git
cd /home/pi/mlat-client
sudo dpkg-buildpackage -b -uc
cd /home/pi/
sudo dpkg -i mlat-client_*.deb
echo -e "\e[32mInstalltion of mlat-client completed.....\e[39m"
## Restart rb24feeder
sudo systemctl restart rbfeeder
fi

## (5) OPENSKY FEEDER INSTALLATION
if [[ ! -z "$MyOSSerial" ]]
then
echo -e "\e[33mInstalling Opensky-network Feeder .....\e[39m"
sudo wget https://opensky-network.org/files/firmware/opensky-feeder_latest_armhf.deb
sudo dpkg -i opensky-feeder_latest_armhf.deb
sudo touch $OS_SERIAL_FILE
sudo chmod 644 $OS_SERIAL_FILE
sudo cat <<EOT > "$OS_SERIAL_FILE"
[Device]
serial = $MyOSSerial


EOT
sudo touch $OS_CONF_FILE
sudo chmod 644 $OS_CONF_FILE
sudo cat <<EOT > "$OS_CONF_FILE"
[GPS]
Latitude=$MyLat
Longitude=$MyLon
Altitude=$MyAlt

[DEVICE]
Type=default

[IDENT]
Username=$MyOSUser

[INPUT]
Host=$MyReceiverHost
Port=$MyReceiverPort

[DEVICE]
Serial=$MyOSSerial

EOT
sudo systemctl restart openskyd
echo -e "\e[32mInstallation of Opensky-network Feeder completed .....\e[39m"
fi

## (6) FR24FEED INSTALLATION
if [[ ! -z "$MyFR24KEY" ]]
then
echo -e "\e[33mInstalling FR24 feeder .....\e[39m"
## Configuration of FR24 feeder
sudo touch $FR24_FILE
sudo chmod 666 $FR24_FILE
sudo cat <<EOT > "$FR24_FILE"
receiver="beast-tcp"
host="$MyReceiverHost:$MyReceiverPort"
fr24key="$MyFR24KEY"

bs="no"
raw="no"
logmode="0"
windowmode="0"
mpx="no"
mlat="yes"
mlat-without-gps="yes"
use-http=yes
http-timeout=20

EOT

## Installation of FR24 feeder
sudo bash -c "$(wget -O - https://repo-feed.flightradar24.com/install_fr24_rpi.sh)"
sudo systemctl restart fr24feed
echo -e "\e[32mInstallation and configuration of FR24 Feeder completed.....\e[39m"
fi

echo -e "\e[31mREBOOT Now \e[39m"