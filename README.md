Script to install ADS-B receiver and feeders:

- dump1090-fa receiver
- Piaware feeder
- FR24 feeder
- RB24 feeder
- OpenSky feeder
- wiedehopf's collectd based reporting tool

Tested on Orange PI Zero 256Mb with Armbian Buster